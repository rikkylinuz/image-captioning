Implemented image captioning for the flickr8k dataset using LSTM generators. Inceptionv3 model is used to encode the images. Used the basic and greedy decoders to predict the image captions.

Google colab link:
https://colab.research.google.com/drive/15loZWDpzqhuahSbeTRTAMIyelbgJC0yV?usp=sharing
